'''
Load monitoring configuration
'''

import json
import os

from term_logger import TermLogger


def load_config():
    '''
    Load fonfiguration
    '''
    default_config = {
        "ports_to_monitor": [8888],
        "report_dir": "/var/monit/reports/",
        "log_dir": "/var/log/monit/",
        "ram_warn_threshold": 90,
        "disk_warn_threshold": 90,
        "cpu_warn_threshold": 90,
    }

    try:
        with open("/etc/monit/config.json", "r", encoding="utf-8") as config_file:
            config = json.load(config_file)
            # Update default values with the values from the config file
            default_config.update(config)
            term_logger = TermLogger(default_config)
            return default_config
    except FileNotFoundError as error:
        term_logger = TermLogger(default_config)
        term_logger.term_error_log(f"Error loading configuration: {error}")
        return default_config


def ensure_data_directory_exists(data_dir):
    '''
    Check if directory exist
    '''
    os.makedirs(data_dir, exist_ok=True)
    return data_dir
