'''
File logger Class file
'''

import logging


class FileLogger:
    '''
    File logger Class
    '''
    def __init__(self, log_file_path):
        self.file_handler = logging.FileHandler(log_file_path)
        file_formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        self.file_handler.setFormatter(file_formatter)

        self.logger_file = logging.getLogger(__name__ + "file")
        self.logger_file.setLevel(logging.DEBUG)
        self.logger_file.addHandler(self.file_handler)

    def file_info_log(self, message):
        '''
        Out an info log
        '''
        self.logger_file.info(message)

    def file_debug_log(self, message):
        '''
        Out an debug log
        Not used
        '''
        self.logger_file.debug(message)

    def file_warning_log(self, message):
        '''
        Out an warn log
        '''
        self.logger_file.warning(message)

    def file_error_log(self, message):
        '''
        Out an error log
        '''
        self.logger_file.error(message)

    def file_critical_log(self, message):
        '''
        Out an critical log
        Not used
        '''
        self.logger_file.critical(message)

    def log_system_status(
        self, timestamp, ram_usage, disk_usage, cpu_activity, tcp_ports_status
        ):
        '''
        Print status
        '''
        self.file_info_log(f"Timestamp: {timestamp}")
        self.file_info_log(f"RAM Usage: {ram_usage}%")
        self.file_info_log(f"Disk Usage: {disk_usage}%")
        self.file_info_log(f"CPU Activity: {cpu_activity}%")
        self.file_info_log("TCP Ports Status:")
        for port, status in tcp_ports_status.items():
            self.file_info_log(f"Port {port}: {'Open' if status else 'Closed'}")
