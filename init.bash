#!/usr/bin/env bash

# Create the user 'monit' if it doesn't exist
if ! id "monit" &>/dev/null; then
    useradd -m -s /bin/bash monit
fi

# Create directories and set permissions
mkdir -p /var/monit/reports /var/log/monit

# Set ownership and permissions for the directories
chown -R monit:monit /var/monit/reports /var/log/monit /etc/monit/
chmod -R 750 /var/monit/reports /var/log/monit /etc/monit/

mkdir -p /usr/local/lib64/monit
cp *.py /usr/local/lib64/monit/

cp api.py /usr/local/bin
cp monit.py /usr/local/bin

chown -R monit:monit /usr/local/lib64/monit
chown monit:monit /usr/local/bin/api.py
chown monit:monit /usr/local/bin/monit.py

chmod 750 /usr/local/bin/api.py
chmod 750 /usr/local/bin/monit.py

echo "export PATH=$PATH:/usr/local/bin" >> /home/monit/.bashrc

pip install -r requirements.txt

# Create conf file and set permissions
mkdir -p /etc/monit
touch /etc/monit/config.json

chown -R monit:monit /etc/monit/
chmod 664 /etc/monit/config.json

echo '{
  "ports_to_monitor": [22, 80, 443, 8080],
  "report_dir": "/var/monit/reports/",
  "log_dir": "/var/log/monit/",
  "ram_warn_threshold": 80,
  "disk_warn_threshold": 80,
  "cpu_warn_threshold": 80
}' > /etc/monit/config.json

echo "User 'monit' created and directories set up."