'''
Args gestion of monitoring tool'''

from sys import exit

from report_tools import (
    calculate_avg_values,
    print_last_report,
    remove_reports,
    print_avg_values,
)


def process_command_line_arguments(report_dir, argv):
    '''
    Get, Clear and Help gestion
    '''
    if "get" in argv:
        if "last" in argv:
            print_last_report(report_dir)
            exit(0)
        elif "avg" in argv:
            if len(argv) >= 4 and argv[3].isdigit():
                hours = int(argv[3])
                print_avg_values(calculate_avg_values(report_dir, hours))
                exit(0)
            else:
                print("Please provide a valid number of hours for average calculation.")
                exit(1)

    if "clear" in argv:
        remove_reports(report_dir)
        exit(0)

    if "help" in argv:
        print_help_and_exit()


def print_help_and_exit():
    '''
    Output for help arg
    '''
    print("Options:")
    print("  check: Get information and save a report.")
    print("  get last: Print the last report.")
    print("  get avg X: Print average values for the last X hours.")
    print("  clear: Remove all reports.")
    exit(0)
