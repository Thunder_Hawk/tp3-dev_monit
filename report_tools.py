'''
Functions for monitoring tool
'''


import os
from glob import glob
import json
import socket
from datetime import datetime, timedelta


def save_report(report, report_file_path, file_logger, term_logger):
    '''
    Save a report
    '''
    with open(report_file_path, "w", encoding="utf-8") as report_file:
        json.dump(report, report_file, indent=2)
        file_logger.file_info_log(f"Report saved to: {report_file_path}")
        term_logger.term_info_log(f"Report saved to: {report_file_path}")
        # term info pls


def check_tcp_ports(ports):
    '''
    Check tcp ports
    '''
    # Check if specified TCP ports are open
    status = {}
    for port in ports:
        try:
            with socket.create_connection(("localhost", port), timeout=1):
                status[port] = True
        except (socket.timeout, ConnectionRefusedError):
            status[port] = False
    return status


def generate_report(
    timestamp, check_id, ram_usage, disk_usage, cpu_activity, tcp_ports_status
):
    '''
    Generate report
    '''
    return {
        "timestamp": timestamp,
        "check_id": check_id,
        "ram_usage": ram_usage,
        "disk_usage": disk_usage,
        "cpu_activity": cpu_activity,
        "tcp_ports_status": tcp_ports_status,
    }


def print_system_status(
    timestamp, ram_usage, disk_usage, cpu_activity, tcp_ports_status
):
    '''
    Print status
    '''
    print(f"Timestamp: {timestamp}")
    print(f"RAM Usage: {ram_usage}%")
    print(f"Disk Usage: {disk_usage}%")
    print(f"CPU Activity: {cpu_activity}%")
    print("TCP Ports Status:")
    for port, status in tcp_ports_status.items():
        print(f"   Port {port}: {'Open' if status else 'Closed'}")


def remove_reports(report_dir):
    '''
    Remove all reports on the report directory
    '''
    # Remove all report files
    report_files = glob(os.path.join(report_dir, "* - report.json"))
    for report_file in report_files:
        os.remove(report_file)
    print("All reports removed.")


def print_avg_reports(report_dir):
    '''
    Calculate average reports values 
    '''
    # Calculate average values from all reports
    report_files = glob(os.path.join(report_dir, "* - report.json"))
    if report_files:
        total_reports = len(report_files)
        total_ram = 0
        total_disk = 0
        total_cpu = 0

        for report_file in report_files:
            with open(report_file, "r", encoding="utf-8") as report:
                data = json.load(report)
                total_ram += data["ram_usage"]
                total_disk += data["disk_usage"]
                total_cpu += data["cpu_activity"]

        avg_ram = round(total_ram / total_reports, 2)
        avg_disk = round(total_disk / total_reports, 0)
        avg_cpu = round(total_cpu / total_reports, 0)

        print(f"Average RAM Usage: {avg_ram}%")
        print(f"Average Disk Usage: {avg_disk}%")
        print(f"Average CPU Activity: {avg_cpu}%")
    else:
        print("No reports found.")


def print_last_report(report_dir):
    '''
    Print last report
    '''
    # Get the latest report file
    report_files = glob(os.path.join(report_dir, "* - report.json"))
    if report_files:
        latest_report_file = max(report_files, key=os.path.getctime)
        with open(latest_report_file, "r", encoding="utf-8") as latest_report:
            print(latest_report.read())
    else:
        print("No reports found.")


def log_warnings(file_logger, term_logger, ram_usage, disk_usage, cpu_activity, config):
    '''
    Generate warn log (using configuration)
    '''
    if ram_usage > config.get("ram_warn_threshold", 90):
        file_logger.file_warning_log(f"High RAM usage detected: {ram_usage}%")
        term_logger.term_warn_log(f"High RAM usage detected: {ram_usage}%")
    if disk_usage > config.get("disk_warn_threshold", 90):
        file_logger.file_warning_log(f"High Disk usage detected: {disk_usage}%")
        term_logger.term_warn_log(f"High Disk usage detected: {disk_usage}%")
    if cpu_activity > config.get("cpu_warn_threshold", 90):
        file_logger.file_warning_log(f"High CPU activity detected: {cpu_activity}%")
        term_logger.term_warn_log(f"High CPU activity detected: {cpu_activity}%")


def load_reports(report_dir):
    '''
    Load reports in RAM
    '''
    reports = []
    for file_name in os.listdir(report_dir):
        if file_name.endswith(".json"):
            file_path = os.path.join(report_dir, file_name)
            with open(file_path, "r", encoding="utf-8") as report_file:
                try:
                    report_data = json.load(report_file)
                    reports.append(report_data)
                except json.JSONDecodeError:
                    print(f"Error decoding JSON in file: {file_path}")
    return reports


def parse_timestamp(timestamp_str):
    '''
    Format parametter as used timestamp format
    '''
    timestamp_format = "%H:%M:%S-%d.%m.%Y"
    return datetime.strptime(timestamp_str, timestamp_format)


def calculate_avg_values(report_dir, hours):
    '''
    Calculate average values of reports between exec and hours passed as parametter before
    loading reports
    '''
    reports = load_reports(report_dir)

    if not reports:
        print("No reports available.")
        return

    end_time = datetime.now()
    start_time = end_time - timedelta(hours=hours)

    relevant_reports = [
        report
        for report in reports
        if start_time <= parse_timestamp(report["timestamp"]) <= end_time
    ]

    if not relevant_reports:
        print(f"No reports available for the last {hours} hours.")
        return

    avg_values = calculate_average(relevant_reports)
    print("Average values for the last", hours, "hours:")
    print_avg_values(avg_values)


def calculate_average(relevant_reports):
    '''
    Calculate average
    '''
    total_reports = len(relevant_reports)
    total_ram = sum(report["ram_usage"] for report in relevant_reports)
    total_disk = sum(report["disk_usage"] for report in relevant_reports)
    total_cpu = sum(report["cpu_activity"] for report in relevant_reports)

    avg_values = {
        "ram_usage": total_ram / total_reports,
        "disk_usage": total_disk / total_reports,
        "cpu_activity": total_cpu / total_reports,
    }

    return avg_values


def print_avg_values(avg_values):
    '''
    Print average values
    '''
    print(f"RAM Usage: {avg_values['ram_usage']}%")
    print(f"Disk Usage: {avg_values['disk_usage']}%")
    print(f"CPU Activity: {avg_values['cpu_activity']}%")
