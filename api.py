#!/usr/bin/env python3
"""
API HTTP REST of the monitoring tool
"""

import json
import os
from datetime import datetime, timedelta
from flask import Flask, abort, jsonify


from report_tools import load_reports, parse_timestamp


APP = Flask(__name__)
APP.secret_key = b"SECRET_KEY"

REPORTS_DIR = "/var/monit/reports/"


@APP.route("/reports", methods=["GET"])
def get_reports():
    """
    Read all files in the REPORTS_DIR
    """
    reports = []

    for filename in os.listdir(REPORTS_DIR):
        if filename.endswith(".json"):
            report_path = os.path.join(REPORTS_DIR, filename)
            with open(report_path, "r", encoding="utf-8") as file:
                report_data = json.load(file)
                reports.append(report_data)

    return jsonify(reports)


@APP.route("/reports/<timestamp>", methods=["GET"])
def get_report(timestamp=None):
    """
    GET a report by his timestamp
    """
    report_file = f"{timestamp} - report.json"
    report_path = os.path.join(REPORTS_DIR, report_file)

    if os.path.exists(report_path):
        with open(report_path, "r", encoding="utf-8") as file:
            report_data = json.load(file)
        return jsonify(report_data)
    abort(404)
    return None


@APP.route("/reports/last", methods=["GET"])
def get_last_report():
    """
    GET the last report
    """
    all_reports = []

    for filename in os.listdir(REPORTS_DIR):
        if filename.endswith(".json"):
            report_path = os.path.join(REPORTS_DIR, filename)
            with open(report_path, "r", encoding="utf-8") as file:
                report_data = json.load(file)
                all_reports.append((report_data["timestamp"], report_data))

    if all_reports:
        last_report = max(
            all_reports, key=lambda x: datetime.strptime(x[0], "%H:%M:%S-%d.%m.%Y")
        )[1]
        return jsonify(last_report)
    abort(404)
    return None

@APP.route("/reports/avg/<int:hours>", methods=["GET"])
def get_average_report(hours):
    """
    GET all reports between the exec
    and a numbers of hours before
    (passed as parametter)

    return the average of datas
    """
    reports = load_reports(REPORTS_DIR)
    end_time = datetime.now()
    start_time = end_time - timedelta(hours=hours)

    relevant_reports = [
        report
        for report in reports
        if start_time <= parse_timestamp(report["timestamp"]) <= end_time
    ]

    if relevant_reports:
        avg_values = calculate_average(relevant_reports)
        return jsonify(avg_values)
    abort(404)
    return None



def calculate_average(relevant_reports):
    """
    Caculate average of reports
    """
    total_reports = len(relevant_reports)
    total_ram = sum(report["ram_usage"] for report in relevant_reports)
    total_disk = sum(report["disk_usage"] for report in relevant_reports)
    total_cpu = sum(report["cpu_activity"] for report in relevant_reports)

    avg_values = {
        "ram_usage": total_ram / total_reports,
        "disk_usage": total_disk / total_reports,
        "cpu_activity": total_cpu / total_reports,
    }

    return avg_values


if __name__ == "__main__":
    APP.run(host="127.0.0.1", port=5000, debug=True)


# regex serating timestamp and id

# seconde api for check
