#!/usr/bin/env python3
'''
__main__ of the monitoring service
'''

import sys
import os
from datetime import datetime
from uuid import uuid4
import psutil

from file_logger import FileLogger
from term_logger import TermLogger
from config_loader import load_config, ensure_data_directory_exists
from report_tools import (
    save_report,
    check_tcp_ports,
    generate_report,
    print_system_status,
    log_warnings,
)
from monit_args import process_command_line_arguments


def gather_system_metrics():
    '''
    Get system informations
    '''
    timestamp = datetime.now().strftime("%H:%M:%S-%d.%m.%Y")
    ram_usage = psutil.virtual_memory().percent
    disk_usage = psutil.disk_usage("/").percent
    cpu_activity = psutil.cpu_percent(interval=1)

    return timestamp, ram_usage, disk_usage, cpu_activity


def monitor_resources():
    '''
    Compare system information with configuration
    '''
    timestamp, ram_usage, disk_usage, cpu_activity = gather_system_metrics()

    config = load_config()

    file_logger = FileLogger("/var/log/monit/monitoring_tool.log")
    term_logger = TermLogger(config)

    ports_to_check = config.get("ports_to_monitor", [])
    tcp_ports_status = check_tcp_ports(ports_to_check)

    check_id = str(uuid4())

    report_dir = config.get("report_dir", "/var/monit/")
    data_dir = ensure_data_directory_exists(report_dir)
    report_file_path = os.path.join(data_dir, f"{timestamp} - report.json")

    if "check" in sys.argv:
        report = generate_report(
            timestamp, check_id, ram_usage, disk_usage, cpu_activity, tcp_ports_status
        )
        save_report(report, report_file_path, file_logger, term_logger)

    if not sys.argv[1:] :
        # file_logger.log_system_status(
            # timestamp, ram_usage, disk_usage, cpu_activity, tcp_ports_status
        # )
        log_warnings(
            file_logger, term_logger, ram_usage, disk_usage, cpu_activity, config
        )
        print_system_status(
            timestamp, ram_usage, disk_usage, cpu_activity, tcp_ports_status
        )

    process_command_line_arguments(report_dir, sys.argv)


if __name__ == "__main__":
    monitor_resources()
