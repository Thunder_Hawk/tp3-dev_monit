'''
TermLogger Class file
'''

import logging
from custom_formatter import CustomFormatter


class TermLogger:
    ''' 
    TermLogger Class
    '''
    def __init__(self, config):
        self.config = config
        self.term_handler = logging.StreamHandler()
        self.term_handler.setFormatter(CustomFormatter())

        self.logger_term = logging.getLogger(__name__ + "term")
        self.logger_term.setLevel(logging.INFO)
        self.logger_term.addHandler(self.term_handler)
        
    def term_info_log(self, message):
        '''
        Generate info log
        '''
        self.logger_term.info(message)

    def term_warn_log(self, message):
        '''
        Generate warn log
        '''
        self.logger_term.warning(message)
        
    def term_error_log(self, message):
        '''
        Generate error log
        '''
        self.logger_term.error(message)

    # def log_system_status(
    #     self, timestamp, ram_usage, disk_usage, cpu_activity, tcp_ports_status
    # ):
    #     '''
    #     Generate warn log
    #     '''
    #     self.term_warn_log(f"Timestamp: {timestamp}")
    #     self.term_warn_log(f"RAM Usage: {ram_usage}%")
    #     self.term_warn_log(f"Disk Usage: {disk_usage}%")
    #     self.term_warn_log(f"CPU Activity: {cpu_activity}%")
    #     self.term_warn_log("TCP Ports Status:")
    #     for port, status in tcp_ports_status.items():
    #         self.term_warn_log(f"Port {port}: {'Open' if status else 'Closed'}")
